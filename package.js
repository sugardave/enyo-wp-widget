var WpInfo = WpInfo || {
	plugin_dir: ""
}

var pd = WpInfo.plugin_dir;

enyo.depends(
	"$lib/onyx",
	"$lib/layout",

	pd + "enyo-wp-widget.css",

	pd + "canvas-spinner/heartcode_canvas.js",
	pd + "canvas-spinner/canvasSpinner.js",

	pd + "panels/Splash.js",
	pd + "App.js"
);