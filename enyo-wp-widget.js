var target = enyo.dom.byId(RenderInfo.enyo_wp_widget_render_target);
var images = RenderInfo.images;

enyo.kind({
	name: "enyo.WpWidget",
	kind: enyo.Control,
	classes: "enyo-unselectable enyo-fill",
	components: [
		{name: "widget", kind: "App"}
	],
	create: function() {
		this.inherited(arguments);
		this.$.widget.setImages(this.images);
	}
});

new enyo.WpWidget({images: images}).renderInto(target);