enyo.kind({
	name: "App",
	classes: "enyo-fill",
	published: {
		images: []
	},
	components: [
		{name: "mainPanel", kind: enyo.Panels, draggable: false, classes: "enyo-fit", components: [
			{name: "splash", kind: "Splash"},
			{name: "carousel", kind: enyo.ImageCarousel, defaultKind: enyo.ImageView, wrap: true},
			//{name: "carousel", kind: enyo.ImageCarousel, wrap: true, style: "height: 225px; width: 300px;", defaultScale: "auto"}
		]}
	],
	create: function() {
		this.inherited(arguments);
	},
	imagesChanged: function() {
		var src;
		this.$.mainPanel.previous();
		//this.$.carousel.setImages(this.images);
		this.$.carousel.destroyClientControls();
		for (var i in this.images) {
			src = this.images[i];
			this.$.carousel.createComponent({src: src, style: "width:300px; height:225px;", owner: this});
		}
		this.start(1750);
	},
	start: function(inTick) {
		if (!inTick) {
			inTick = 0;
		}

		setTimeout(enyo.bind(this, function() {
			this.$.mainPanel.next();
			this.rotate();
		}), inTick);
	},
	rotate: function() {
		if (this.heartbeat) {
			clearInterval(this.heartbeat);
		}

		this.heartbeat = setInterval(enyo.bind(this.$.carousel, "next"), 7000);
	}
});