enyo.kind({
	name: "Splash",
	kind: enyo.FittableRows,
	components: [
		//{fit: true},
		{style: "height: 30%;"},
		{kind: enyo.FittableColumns, fit: true, classes: "enyo-center", components: [
			{kind: "jmtk.Spinner"}
		]},
		{kind: enyo.FittableColumns, classes: "enyo-center", components: [
			{content: "Loading images...", classes: "sub-heading"}
		]}
	]
});